#include <Wire.h>
#include <TimerOne.h>
 
#define MPU9250_ADDRESS 0x68
#define MAG_ADDRESS 0x0C
 
#define GYRO_FULL_SCALE_250_DPS 0x00 
#define GYRO_FULL_SCALE_500_DPS 0x08
#define GYRO_FULL_SCALE_1000_DPS 0x10
#define GYRO_FULL_SCALE_2000_DPS 0x18
 
#define ACC_FULL_SCALE_2_G 0x00 
#define ACC_FULL_SCALE_4_G 0x08
#define ACC_FULL_SCALE_8_G 0x10
#define ACC_FULL_SCALE_16_G 0x18


#define clk 8
#define AB 9
#define btn1 10
#define btn2 11
#define pot A2
//Variable para la lectura del puerto serial
String inputString;

byte num2Byte(char num){
  byte numBin=B11111111;
  switch (num){
   case '0':
            //abcdefg.
      numBin=B00000011;
      break;
   case '1':
            //abcdefg.
      numBin=B10011111;
      break;
   case '2':
           //abcdefg.
      numBin=B00100101;
      break;
   case '3':
      numBin=B00001101;
      break;
   case '4':
      numBin=B10011001;
      break;
   case '5':
      numBin=B01001001;
      break;
   case '6':
      numBin=B01000001;
      break;
   case '7':  
            //abcdefg.
      numBin=B00011111;
      break;
   case '8':
            //abcdefg.
      numBin=B00000001;
      break;
   case '9':
            //abcdefg.
      numBin=B00011001;
      break;
  }
  return numBin;
}

// This function read Nbytes bytes from I2C device at address Address. 
// Put read bytes starting at register Register in the Data array. 
void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.endTransmission();
   
  // Read Nbytes
  Wire.requestFrom(Address, Nbytes); 
  uint8_t index=0;
  while (Wire.available())
  Data[index++]=Wire.read();
}
 
 
// Write a byte (Data) in device (Address) at register (Register)
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
  // Set register address
  Wire.beginTransmission(Address);
  Wire.write(Register);
  Wire.write(Data);
  Wire.endTransmission();
}
 
// Initial time
long int ti;
volatile bool intFlag=false;
 
// Initializations
void setup()
{
  // Arduino initializations
  Wire.begin();
  Serial.begin(115200);
 
  // Set accelerometers low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS,29,0x06);
  // Set gyroscope low pass filter at 5Hz
  I2CwriteByte(MPU9250_ADDRESS,26,0x06);
 
 
  // Configure gyroscope range
  I2CwriteByte(MPU9250_ADDRESS,27,GYRO_FULL_SCALE_1000_DPS);
  // Configure accelerometers range
  I2CwriteByte(MPU9250_ADDRESS,28,ACC_FULL_SCALE_4_G);
  // Set by pass mode for the magnetometers
  I2CwriteByte(MPU9250_ADDRESS,0x37,0x02);
   
  // Request continuous magnetometer measurements in 16 bits
  I2CwriteByte(MAG_ADDRESS,0x0A,0x16);
   
  Timer1.initialize(10000); // initialize timer1, and set a 1/2 second period
  Timer1.attachInterrupt(callback); // attaches callback() as a timer overflow interrupt
  pinMode(clk,OUTPUT);
  pinMode(AB,OUTPUT);
  pinMode(13,OUTPUT);
  pinMode(btn1, INPUT);
  pinMode(btn2, INPUT);
  pinMode(pot, INPUT);
  for(int i = 2; i<8; i++){
    pinMode(i,OUTPUT);
  }
 
  // Store initial time
  ti=millis();
}
 
// Counter
long int cpt=0;
 
void callback()
{ 
  intFlag=true;
  digitalWrite(13, digitalRead(13) ^ 1);
}
 
// Main loop, read and display data
void loop()
{
//btn1=1,btn2=1,pot=0~100,posX=12,posY=-139;
  String message;
  readBtns(&message);
  readPot(&message);
  readMPU(&message);
  delay(100);
  message+=";";
  Serial.println(message);
}

void readBtns(String *message){
  *message += "btn1=";
  if(digitalRead(btn1)){*message += "1";}
  else{*message += "0";}
  *message += ",btn2=";
  if(digitalRead(btn2)){*message += "1";}
  else{*message += "0";}
}

void readPot (String* mns){
  *mns += ",pot=";
  int value = analogRead(pot);
  value = map(value, 0, 1024, 0, 100);
  *mns += String(value);
}
void readMPU(String* mns){
     
  while (!intFlag);
  intFlag=false;
     
   
  // ____________________________________
  // ::: accelerometer and gyroscope :::
   
  // Read accelerometer and gyroscope
  uint8_t Buf[14];
  I2Cread(MPU9250_ADDRESS,0x3B,14,Buf);
   
  // Create 16 bits values from 8 bits data
   
  // Accelerometer
  int16_t ax=-(Buf[0]<<8 | Buf[1]);
  int16_t ay=-(Buf[2]<<8 | Buf[3]);
  int16_t az=Buf[4]<<8 | Buf[5];
   
  // Gyroscope
  int16_t gx=-(Buf[8]<<8 | Buf[9]);
  int16_t gy=-(Buf[10]<<8 | Buf[11]);
  int16_t gz=Buf[12]<<8 | Buf[13];
  *mns+=",posx=";
  String pitch = String(gy);
  *mns+=pitch;
  *mns+=",posy=";
  String roll = String(gz);
  *mns+=roll;
  // _____________________
  // ::: Magnetometer ::: 
  // Read register Status 1 and wait for the DRDY: Data Ready
    uint8_t ST1;
    do
    {
      I2Cread(MAG_ADDRESS,0x02,1,&ST1);
    }
    while (!(ST1&0x01));
   
    // Read magnetometer data 
    uint8_t Mag[7]; 
    I2Cread(MAG_ADDRESS,0x03,7,Mag);
     
    // Create 16 bits values from 8 bits data
     
    // Magnetometer
    int16_t mx=-(Mag[3]<<8 | Mag[2]);
    int16_t my=-(Mag[1]<<8 | Mag[0]);
    int16_t mz=-(Mag[5]<<8 | Mag[4]);  
}

 
void showValues(char leds[7],char num){
  shiftOut(AB, clk, LSBFIRST,num2Byte(num));
  for(int i = 0; i<6;i++){
    bool OnOff = false;
    if(leds[i]=='1'){OnOff = true;} 
    digitalWrite(i+2,OnOff);
  }
}

void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == ';') {
        Serial.println(inputString);
        char energia [7];
        inputString.substring(5,13).toCharArray(energia,7);
        Serial.println(energia);
        char restantes = (inputString.substring(20,21))[0];
        showValues(energia,restantes);
        inputString="";
    }
  }
}
 
